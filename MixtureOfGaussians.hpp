#include <vector>
#include <opencv2/opencv.hpp>
#include "Gaussian.hpp"
using namespace std;
using namespace cv;


class MixtureOfGaussians{
    public:
        // Number of Gaussian Components
        int K;

        bool isDebug = false;

        // Vector of gaussian distributions
        vector<Gaussian> gaussians;

        // Learning rate/Update factor
        double alpha = 0.015;

        // When there's no match for any of the current Gaussian distributions the least probable
        // one is replaced based on the current observation. The paper states that we use the
        // current value as the mean and "an initially high variance" defined below. A good value
        // might be very dependent on the domain of the problem we're modelling.
        // The variance is also limited so we don't get crazy big numbers, and hard to match distributions.
        double highSigma = 36.0;
        double minSigma = 4.0;

        MixtureOfGaussians(int kGaussians, bool isDebugArg = false) {
            K = kGaussians;
            isDebug = isDebugArg;
        }

        static int weightCompare(const void *_g1, const void *_g2){
            Gaussian *g1 = (Gaussian*) _g1;
            Gaussian *g2 = (Gaussian*) _g2;

            if (g1->omega < g2->omega){
                return 1;
            } else if (g1->omega == g2->omega){
                return 0;
            } else {
                return -1;
            }
        }

        void init(vector<uchar> initialPixelValues) {

            vector<uchar> distinctSamples = {};

            for (int i = 0; i < initialPixelValues.size(); i++)
                if ( std::find(distinctSamples.begin(), distinctSamples.end(), initialPixelValues[i]) == distinctSamples.end())
                    distinctSamples.push_back(initialPixelValues[i]);
            
            //assert(("Not enough samples", K < distinctSamples.size()));
            while(K > distinctSamples.size()) {
                distinctSamples.push_back(distinctSamples.at(distinctSamples.size() - 1));
            }


            // Create K Gaussians
            for(int k = 0; k < K; k++) {
                
                Gaussian g;
                g.omega = 1.0/K;


                g.mi = (double) distinctSamples.at(k);

                double variance = 0.0;
                for(int i = 0; i < initialPixelValues.size(); ++i)
                    variance += pow(initialPixelValues.at(i) - g.mi, 1) / initialPixelValues.size(); 
                g.sigma = 6;
                
                gaussians.push_back(g);
            }


            // Reorder Gaussians by weight
            qsort(gaussians.data(), K, sizeof(Gaussian), weightCompare );
        }


        int onlineAverageORIGO(uchar x) {
            double x_t = (double) x;
            if(isDebug) printf("\nNew value %d \n", x);

            double omega_sum = 0.0;

            for(int k = 0; k < K; k++) {
                
                Gaussian g = gaussians.at(k);

                g.omega = (1 - alpha) * g.omega + alpha * g.pdf(x_t);
                omega_sum += g.omega;
                g.rho = (alpha * g.pdf(x_t)) / g.omega;
                g.mi = (1 - g.rho) * g.mi + g.rho * x_t;

                g.sigma = (1 - g.rho) * g.sigma + g.rho * pow(x_t - g.mi, 2);

                g.sigma = (g.sigma < 1.0) ? 1.0 : g.sigma;
                g.sigma = (g.sigma > 30.0) ? 30.0 : g.sigma;

                
                gaussians[k] = g;

                if(isDebug) printf("Gaussian k=%d, μ=%f, σ=%f, ω=%f, \n", k, g.mi, g.sigma, g.omega);
            }
            
            for(int k = 0; k < K; k++)
                gaussians[k].omega = gaussians[k].omega / omega_sum;

            qsort(gaussians.data(), K, sizeof(Gaussian), weightCompare );





            double best_prob = 0.0;
            for(int k = 0; k < K; k++)
                if(gaussians.at(k).omega * gaussians.at(k).pdf(x_t) > best_prob)
                    best_prob = gaussians.at(k).omega * gaussians.at(k).pdf(x_t);

            if(isDebug) printf("Best probability match is %f\n", best_prob);

            if(best_prob < 0.00001) {
                Gaussian g_last = gaussians.at(K - 1);

                Gaussian g_new;
                g_new.omega = g_last.omega;
                g_new.mi = x_t;
                g_new.sigma = minSigma;

                gaussians.at(K - 1) = g_new;
                if(isDebug) printf("New Gaussian k=%d, μ=%f, σ=%f, ω=%f, \n", K - 1, g_new.mi, g_new.sigma, g_new.omega);
            } 
        }



        int onlineAverage(uchar x) {
            double x_t = (double) x;
            if(isDebug) printf("\nNew value %d \n", x);


            double best_prob = 0.0;
            int best_prob_k = matchValueToGaussian(x, best_prob);

            if(isDebug) printf("Best probability match is %f\n", best_prob);


            // Should new gaussian ?
            if(best_prob < 0.01) {
                Gaussian g_last = gaussians.at(K - 1);

                Gaussian g_new;
                g_new.omega = g_last.omega;
                g_new.mi = x_t;
                g_new.sigma = minSigma;

                gaussians.at(K - 1) = g_new;
                if(isDebug) printf("New Gaussian k=%d, μ=%f, σ=%f, ω=%f, \n", K - 1, g_new.mi, g_new.sigma, g_new.omega);
            } 





            double omega_sum = 0.0;

            for(int k = 0; k < K; k++) {
                
                Gaussian g = gaussians.at(k);

                if(k == best_prob_k) {

                    g.omega = (1 - alpha) * g.omega + alpha * g.pdf(x_t);
                    
                    g.rho = (alpha * g.pdf(x_t)) / g.omega;
                    g.mi = (1 - g.rho) * g.mi + g.rho * x_t;

                    g.sigma = (1 - g.rho) * g.sigma + g.rho * pow(x_t - g.mi, 2);

                    g.sigma = (g.sigma < 1.0) ? 1.0 : g.sigma;
                    g.sigma = (g.sigma > 30.0) ? 30.0 : g.sigma;

                } else {
                    g.omega = (1 - alpha) * g.omega;
                }
                
                omega_sum += g.omega;
                gaussians[k] = g;

                if(isDebug) printf("Gaussian k=%d, μ=%f, σ=%f, ω=%f, \n", k, g.mi, g.sigma, g.omega);
            }
            
            for(int k = 0; k < K; k++)
                gaussians[k].omega = gaussians[k].omega / omega_sum;

            qsort(gaussians.data(), K, sizeof(Gaussian), weightCompare );

        }


        int matchValueToGaussian(uchar x, double & best_prob) {
            double x_t = (double) x;
            
            //double best_prob = 0.0;
            int best_prob_k = 0;

            for(int k = 0; k < K; k++) {
                Gaussian g = gaussians.at(k);

                double temp_prob = g.omega * g.pdf(x_t);
                
                if(temp_prob > best_prob) {
                    best_prob = temp_prob;
                    best_prob_k = k;
                }
            }

            return best_prob_k;
        }

        bool predictIsBackground(uchar x, double bgTreshold) {
            double x_t = (double) x;
            
            double best_prob = 0.0;
            int best_prob_k = matchValueToGaussian(x, best_prob);

            if(isDebug) printf("Best probability match is k = %d with confidence %f\n", best_prob_k, best_prob);


            vector<int> possible_k_bg = {};
            double omega_sum = 0.0;

            for(int k = K - 1; k >= 0; k--) {
                Gaussian g = gaussians.at(k);

                omega_sum += g.omega;

                if(omega_sum > bgTreshold) 
                    possible_k_bg.push_back(k);
            }



            bool isBackground = std::find(possible_k_bg.begin(), possible_k_bg.end(), best_prob_k) != possible_k_bg.end();

            if(isDebug)
                if(isBackground)
                    printf("Value %d is background\n", x);
                else 
                    printf("Value %d is NOT background\n", x);

            return isBackground;
        }
};