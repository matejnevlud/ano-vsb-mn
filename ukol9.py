import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import numpy as np


train_data = ((0.111, 0.935), (0.155, 0.958), (0.151, 0.960), (0.153, 0.955),  # # - square
              (0.715, 0.924), (0.758, 0.964), (0.725, 0.935), (0.707, 0.913),  # * - star
              (0.167, 0.079), (0.215, 0.081), (0.219, 0.075), (0.220, 0.078),) # ## - rectangle

train_labels = ((1.0, 0.0, 0.0), (1.0, 0.0, 0.0), (1.0, 0.0, 0.0), (1.0, 0.0, 0.0), # # - square
              (0.0, 1.0, 0.0), (0.0, 1.0, 0.0), (0.0, 1.0, 0.0), (0.0, 1.0, 0.0),   # * - star
              (0.0, 0.0, 1.0), (0.0, 0.0, 1.0), (0.0, 0.0, 1.0), (0.0, 0.0, 1.0),)  # ## - rectangle

test_data = ((0.115, 0.995), (0.152, 1.000), (0.135, 0.993),  # # - square
              (0.694, 0.943), (0.723, 0.935), (0.742, 0.956),  # * - star
              (0.209, 0.080), (0.120, 0.075), (0.158, 0.073)) # ## - rectangle

test_data2 = ((0.120, 0.075), (0.152, 1.000), (0.742, 0.956), # rec, sq, star
               (0.135, 0.993), (0.115, 0.995), (0.209, 0.080), # sq, sq, rec
               (0.158, 0.073), (0.723, 0.935), (0.694, 0.943))  # rec, star, star





class MyDataset(Dataset):
  def __init__(self, features, labels):
        self.labels = labels
        self.features = features

  def __len__(self):
        return len(self.features)

  def __getitem__(self, index):
        X = self.features[index]
        y = self.labels[index]

        return X, y


class FeedforwardNeuralNetModel(torch.nn.Module):
    def __init__(self):
        super(FeedforwardNeuralNetModel, self).__init__()
       # self.il = nn.Linear(2, 10)
      #  self.ol = nn.Linear(10, 3)
       # self.relu = nn.ReLU()	
        self.network = nn.Sequential(
            nn.ReLU(),
            nn.Linear(2,50),    
            nn.Linear(50, 100),
            nn.Linear(100, 50), 
            nn.Linear(50,3),
            nn.Sigmoid(),
        )

    def forward(self, x):
        return self.network(x)
  	#hidden = self.il(x)
   #	activation = self.relu(hidden)
   #	output = self.ol(activation)

def train(dataloader, model):
    train_loss = 0.0
    model.train()


def validation(model):
    model.eval()


tensor_x = torch.stack([torch.Tensor(i) for i in train_data])
tensor_y = torch.stack([torch.Tensor(i) for i in train_labels])    

dataset_train = MyDataset(tensor_x, tensor_y)

dataloader_train = DataLoader(dataset_train, batch_size=4, shuffle=True)

print("ble")
model = FeedforwardNeuralNetModel()
criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=0.05)


# create model


#train(dataloader_train, model)
#validation(model)¨


for epoch in range(200):  # loop over the dataset multiple times
    running_loss = 0.0
    for i, data in enumerate(dataloader_train, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
# zero the parameter gradients
        optimizer.zero_grad()
# forward + backward + optimize
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
       
# print statistics
        running_loss += loss.item()
        if i % 10 == 0:    # print every 2000 mini-batches
            print('epoch: %3d loss: %.10f' %
                  (epoch + 1, running_loss / 10))
            running_loss = 0.0
print('Finished Training')

tensor_test = torch.stack([torch.Tensor(i) for i in test_data])
tensor_test2 = torch.stack([torch.Tensor(i) for i in test_data2])

print("\n-------------TEST 1-----------------\n")
for v in tensor_test:
    pred = model(v)
    nnn = pred.detach().numpy()

    shape = ""
    sh = np.argmax(nnn, axis=0)
    if sh == 0:
        shape="Square"
    elif sh == 1:
        shape="Star"
    else:
        shape="Rectangle"
    
    print("Predicted shape: " + shape)
    print("Predicted result:")
    print(nnn)
    print("\n")

print("\n-------------TEST 2-----------------\n")
for v in tensor_test2:
    pred = model(v)
    nnn = pred.detach().numpy()

    shape = ""
    sh = np.argmax(nnn, axis=0)
    if sh == 0:
        shape="Square"
    elif sh == 1:
        shape="Star"
    else:
        shape="Rectangle"
    
    print("Predicted shape: " + shape)
    print("Predicted result:")
    print(nnn)
    print("\n")