#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <vector>
#include "backprop.h"

#define LAMBDA 1.0
#define ETA 0.1

#define SQR( x ) ( ( x ) * ( x ) )

void randomize( double * p, int n ) 
{
	for ( int i = 0; i < n; i++ ) {
		p[i] = ( double )rand() / ( RAND_MAX );
	}
}


NN * createNN( int n, int h, int o ) 
{
	srand(time(NULL));
	NN * nn = new NN;
	
    nn->n = new int[3];
	nn->n[0] = n;
	nn->n[1] = h;
	nn->n[2] = o;
	nn->l = 3;

	nn->w = new double ** [nn->l - 1];
    

	for ( int k = 0; k < nn->l - 1; k++ ) 
    {
		nn->w[k] = new double * [nn->n[k + 1]];
		for ( int j = 0; j < nn->n[k + 1]; j++ ) 
        {
			nn->w[k][j] = new double[nn->n[k]];			
			randomize( nn->w[k][j], nn->n[k]);
			// BIAS
			//nn->w[k][j] = new double[nn->n[k] + 1];			
			//randomize( nn->w[k][j], nn->n[k] + 1 );
		}		
	}

	nn->y = new double * [nn->l];
	for ( int k = 0; k < nn->l; k++ ) {
		nn->y[k] = new double[nn->n[k]];
		memset( nn->y[k], 0, sizeof( double ) * nn->n[k] );
	}

	nn->in = nn->y[0];
	nn->out = nn->y[nn->l - 1];

	nn->d = new double * [nn->l];
	for ( int k = 0; k < nn->l; k++ ) {
		nn->d[k] = new double[nn->n[k]];
		memset( nn->d[k], 0, sizeof( double ) * nn->n[k] );
	}

	return nn;
}

void releaseNN( NN *& nn ) 
{
	for ( int k = 0; k < nn->l - 1; k++ ) {
		for ( int j = 0; j < nn->n[k + 1]; j++ ) {
			delete [] nn->w[k][j];
		}
		delete [] nn->w[k];
	}
	delete [] nn->w;
		
	for ( int k = 0; k < nn->l; k++ ) {
		delete [] nn->y[k];
	}
	delete [] nn->y;
	
	for ( int k = 0; k < nn->l; k++ ) {
		delete [] nn->d[k];
		
	}
	delete [] nn->d;

	delete [] nn->n;

	delete nn;
	nn = NULL;
}


double neuron_activation( double * weights, double *inputs, int number_of_inputs) {
	double activation = 0.0;
	double bias = 1.0;

	for(int i = 0; i < number_of_inputs; i++) {
		activation += weights[i] * inputs[i];
	}

	return activation + bias;
}

double transfer(double activation) {
	// Sigmoid
	return 1.0 / (1.0 + exp(-1.0 * LAMBDA * activation));
}

void feedforward( NN * nn ) 
{ 
	// i -> 0 - n
	// w -> layer [0, 1] -> neuron [0, 1, 2] -> i-th dimension [0, i]
	double a1 = nn->w[0][0][0];
	double a2 = nn->w[0][0][1];
	double a3 = nn->w[0][0][2];
	
	
	// For nad vrstvami
	for (int k = 1; k < nn->l; k++) {

		// For nad neurony ve vrstvě
		for(int i = 0; i < nn->n[k]; i++){
			double s_k_i = 0.0;
			
			for(int j = 0; j < nn->n[k - 1]; j++) {
				s_k_i += nn->w[k - 1][i][j] * nn->y[k - 1][j];
			}

			//double activation = neuron_activation(nn->w[k][i], inputs, nn->n[k]);
			//nn->y[k][i] = transfer(s_k_i);
			//new_inputs[i] = nn->y[k][i];

			double y_k_i = 1.0 / (1.0 + exp(-1.0 * LAMBDA * s_k_i));
			nn->y[k][i] = y_k_i;
			
		}
	}
}

double backpropagation( NN * nn, double * t ) {
	double error = 0.0;

	// Calculate ERROR
	for (int i = 0; i < 2; i++){
		error += powf(t[i] - nn->y[2][i], 2);
	}
	error *= 0.5f;

	// Calculate deltas for weight correction
	int output_k = nn->l - 1;
	for (int k = nn->l - 1; k >= 0; k--) {
		for(int i = 0; i < nn->n[k]; i++){

			double d_k_i = 0.0;

			if(output_k == k) {
				d_k_i += (t[i] - nn->y[k][i]) * LAMBDA * (1.0f - nn->y[k][i]);
			} else {
				for(int j = 0; j < nn->n[k + 1]; j++) {
					d_k_i += nn->d[k + 1][j] * nn->w[k][j][i]; // nn->w[k][i][j]
				}
				d_k_i *= LAMBDA * nn->y[k][i] * (1.0f - nn->y[k][i]);
			}
			
			nn->d[k][i] = d_k_i;
		}
	}

	// Calculate new weights with computed deltas
	for (int k = 0; k < nn->l - 1; k++) {
		for(int i = 0; i < nn->n[k + 1]; i++) { // Tím indexem [k + 1] si nejsem vůůůbec jistý
			for(int j = 0; j < nn->n[k]; j++) {
				nn->w[k][i][j] += ETA * nn->d[k + 1][i] * nn->y[k][j];
			}
		}
	}

	return error;
}

void setInput( NN * nn, double * in, bool verbose ) 
{
	memcpy( nn->in, in, sizeof( double ) * nn->n[0] );

	if ( verbose ) {
		printf( "input=(" );
		for ( int i = 0; i < nn->n[0]; i++ ) {
			printf( "%0.3f", nn->in[i] );
			if ( i < nn->n[0] - 1 ) {
				printf( ", " );
			}
		}
		printf( ")\n" );
	}
}

int getOutput( NN * nn, bool verbose ) 
{	
    double max = 0.0;
    int max_i = 0;
    if(verbose) printf( " output=" );
	for ( int i = 0; i < nn->n[nn->l - 1]; i++ ) 
    {
		if(verbose) printf( "%0.3f ", nn->out[i] );
        if(nn->out[i] > max) {
            max = nn->out[i];
            max_i = i;
        }
	}
	if(verbose) printf( " -> %d\n" , max_i);
    if(nn->out[0] > nn->out[1] && nn->out[0] - nn->out[1] < 0.1) return 2;
    return max_i;
}
