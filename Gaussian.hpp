#include <cmath>
#include <iostream>
#include <vector>
#include <random>
#include <numeric>
#include <algorithm>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class Gaussian{
    public:

        // Průměrná hodnota
		double mi;

        // Směrodatná odchylka
        double sigma;

        // Váha gausíánu
        double omega;

        // Pomocný výpočet
        double rho;


        double weightStdRatio; // Used to sort the distributions

        Gaussian() {};
        double pdf(double x) {
			return (1.0 / (sigma * sqrt(2 * M_PI))) *
				exp(- pow(x - mi, 2) / (2 * sigma * sigma));

			return (1.0 / sqrt(2 * M_PI * sigma)) *
				exp(-x / (2 * sigma));
		}
};