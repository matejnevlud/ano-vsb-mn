#include <iostream>
#include <thread>
#include <random>
#include <math.h>
#include <opencv2/opencv.hpp>
//#include "opencv4/opencv2/opencv.hpp"

#include "MixtureOfGaussians.hpp"

#include "backprop.h"

using namespace cv;
using namespace std;

std::random_device rd;
std::mt19937 eng(rd());
std::uniform_real_distribution<> uniform_distribution(0.0f, 1.0f);

float randZeroToOne()
{
    return (float)( rand() / (RAND_MAX + 1.));
}

int randOneToN(int N) {
    return (int)( rand() / (RAND_MAX / N - 1));
}



int square_ids[] = {1, 2, 3, 4};
int star_ids[] = {5, 6, 7, 8};
int rectangle_ids[] = {9, 10, 11, 12};

cv::Vec3f square_brush = cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne());
cv::Vec3f star_brush = cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne());
cv::Vec3f rectangle_brush = cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne());


vector<Vec3f> colorsArray = {square_brush, star_brush, rectangle_brush};
vector<Vec3f> randomColors10 = {
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne()),
	cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne())
};

Mat tresholding(const Mat & input) {

	Mat output = Mat (input.rows, input.cols, CV_32FC1);
	for (int row = 0; row < input.rows; row++)
	{
		for (int col = 0; col < input.cols; col++)
		{
			uchar val = input.at<uchar>(row, col);
			if (val >= 150)
				output.at<float>(row, col) = 1.0;
			
		}
	}

	return output;


}

bool flood_neighbours(Mat & input, Mat & color, int x, int y, Vec3f brush, int o_id, Mat &objects_id_image){


	//color.at<cv::Vec3f>(position) = color.at<cv::Vec3f>(position);
	input.at<float>(y, x) = 0.f;
	color.at<cv::Vec3f>(y, x) = brush;
	objects_id_image.at<uchar>(y, x) = o_id;


	if(input.at<float>(y - 1, x) == 1.0f)
		flood_neighbours(input, color, x, y - 1, brush, o_id, objects_id_image);
	if(input.at<float>(y + 1, x) == 1.0f)
		flood_neighbours(input, color, x, y + 1, brush, o_id, objects_id_image);
	if(input.at<float>(y, x - 1) == 1.0f)
		flood_neighbours(input, color, x - 1, y, brush, o_id, objects_id_image);
	if(input.at<float>(y, x + 1) == 1.0f)
		flood_neighbours(input, color, x + 1, y, brush, o_id, objects_id_image);

	//CORNERS
	if(input.at<float>(y - 1, x - 1) == 1.0f)
		flood_neighbours(input, color, x - 1, y - 1, brush, o_id, objects_id_image);
	if(input.at<float>(y + 1, x + 1) == 1.0f)
		flood_neighbours(input, color, x + 1, y + 1, brush, o_id, objects_id_image);
	if(input.at<float>(y + 1, x - 1) == 1.0f)
		flood_neighbours(input, color, x - 1, y + 1, brush, o_id, objects_id_image);
	if(input.at<float>(y - 1, x + 1) == 1.0f)
		flood_neighbours(input, color, x + 1, y - 1, brush, o_id, objects_id_image);
		
		
	return true;
}


Mat index_flood(Mat & input, Mat &objects_id_image) {

	Mat output = Mat (input.rows, input.cols, CV_32FC1);
	Mat color = Mat (input.rows, input.cols, CV_32FC3);

	int o_id = 1;	

	for (int y = 0; y < input.rows; y++)
	{
		for (int x = 0; x < input.cols; x++)
		{
			float val = input.at<float>(y, x);
			cv::Vec2i pos = cv::Point(x, y);
			if (val == 1.f){

				// First hit point in new object
				cv::Vec3f brush = cv::Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne());
				flood_neighbours(input, color, x, y, brush, o_id, objects_id_image);
				o_id++;
			}
		}
	}

	return color;

}

int calculate_moment(Mat & objects_id_image, int o_id, int p, int q) {
	int return_sum = 0;

	for (int y = 0; y < objects_id_image.rows; y++) {
		for (int x = 0; x < objects_id_image.cols; x++) {

			float val = objects_id_image.at<uchar>(y, x);
			if (val == o_id){
				return_sum += pow(x, p) * pow(y, q);
			}
		}
	}

	return return_sum;
}

cv::Point2f calculate_center_of_mass(Mat & objects_id_image, int o_id) {
	int m00 = calculate_moment(objects_id_image, o_id, 0, 0);
	int m10 = calculate_moment(objects_id_image, o_id, 1, 0);
	int m01 = calculate_moment(objects_id_image, o_id, 0, 1);

	float x_t = m10 / m00;
	float y_t = m01 / m00;
	
	return cv::Point2f(x_t, y_t);
}

int calculate_moment_related_to_center_of_mass(Mat & objects_id_image, int o_id, int p, int q) {

	cv::Point2f center_of_mass = calculate_center_of_mass(objects_id_image, o_id);

	int return_sum = 0;

	for (int y = 0; y < objects_id_image.rows; y++) {
		for (int x = 0; x < objects_id_image.cols; x++) {

			float val = objects_id_image.at<uchar>(y, x);
			if (val == o_id){
				return_sum += pow(x - center_of_mass.x, p) * pow(y - center_of_mass.y, q);
			}
		}
	}

	return return_sum;
}

int calculate_circumference(Mat & objects_id_image, int o_id) {

	int return_sum = 0;
	int area_sum = 0;
	int circumference_sum = 0;

	for (int y = 0; y < objects_id_image.rows; y++) {
		for (int x = 0; x < objects_id_image.cols; x++) {

			int val = objects_id_image.at<uchar>(y, x);
			if (val == o_id){
				if(	objects_id_image.at<uchar>(y - 1, x) == 0 ||		
					objects_id_image.at<uchar>(y + 1, x) == 0 ||
					objects_id_image.at<uchar>(y, x - 1) == 0 ||
					objects_id_image.at<uchar>(y, x + 1) == 0) {
						circumference_sum++;
					} else {
						area_sum++;
					}
			}
		}
	}

	return circumference_sum;
}

float feature_1(Mat & objects_id_image, int o_id) {
	int circumference = calculate_circumference(objects_id_image, o_id);
	return circumference * circumference / (100.f * calculate_moment_related_to_center_of_mass(objects_id_image, o_id, 0, 0));
}

float feature_2(Mat & objects_id_image, int o_id) {
	
	int mi_11 = calculate_moment_related_to_center_of_mass(objects_id_image, o_id, 1, 1);
	int mi_20 = calculate_moment_related_to_center_of_mass(objects_id_image, o_id, 2, 0);
	int mi_02 = calculate_moment_related_to_center_of_mass(objects_id_image, o_id, 0, 2);


	float mi_min = 0.5f * (mi_20 + mi_02) - 0.5f * sqrt(4 * pow(mi_11, 2) + pow(mi_20 - mi_02, 2));
	float mi_max = 0.5f * (mi_20 + mi_02) + 0.5f * sqrt(4 * pow(mi_11, 2) + pow(mi_20 - mi_02, 2));

	return mi_min / mi_max;
}



void exercise_2(Mat & input_image) {

	Mat tresholded_image = tresholding(input_image);

	namedWindow("Tresholded", WINDOW_NORMAL);
    imshow( "Tresholded", tresholded_image);

	waitKey( 0 ); // wait until keypressed

	Mat objects_id_image = Mat::zeros(tresholded_image.rows, tresholded_image.cols, CV_8UC1);
	
    Mat indexed_image = index_flood(tresholded_image, objects_id_image);

	int number_of_objects;
	double temp;
	minMaxLoc( objects_id_image, nullptr, &temp, nullptr, nullptr );


	namedWindow("Indexed", WINDOW_NORMAL);
    imshow( "Indexed", objects_id_image);

	printf("Number of objects %f\n", number_of_objects);

	//printf("Moment for id 1 = %d\n", calculate_moment(objects_id_image, 1, 0, 0));
	//printf("Center for id 1 = (%f, %f)\n", calculate_center_of_mass(objects_id_image, 1).x, calculate_center_of_mass(objects_id_image, 1).y);
	
	for( int i = 1; i <= 12; i++){
		printf("Moment for id %d = %d\n", i, calculate_moment(objects_id_image, i, 0, 0));
		printf("Circumference for id %d = %d\n", i, calculate_circumference(objects_id_image, i));
		printf("Feature 1 for id %d = %f\n", i, feature_1(objects_id_image, i));
		printf("Feature 2 for id %d = %f\n\n", i, feature_2(objects_id_image, i));
	}

	//waitKey( 0 ); // wait until keypressed

}


class Etalon {
	public:
		string name;
		Point2f value = Point2f(0,0);
		vector<int> train_oids = {};
		Vec3f brush = Vec3f(randZeroToOne(), randZeroToOne(), randZeroToOne());

};

vector<Etalon> Train_etalons(Mat & objects_id_image) {
	
	Etalon ethalon_square;
	ethalon_square.name = "square";
	ethalon_square.train_oids = {1, 2, 3, 4};
	int square_count = std::end(square_ids) - std::begin(square_ids);
	for( int i = 0; i < square_count; i++){

		int id = ethalon_square.train_oids[i];
		ethalon_square.value.x += feature_1(objects_id_image, id) / square_count;
		ethalon_square.value.y += feature_2(objects_id_image, id) / square_count;
	}
	printf("ethalon_square = (%f, %f)\n", ethalon_square.value.x, ethalon_square.value.y);


	Etalon ethalon_star;
	ethalon_star.name = "star";
	ethalon_star.train_oids = {5, 6, 7, 8};
	int star_count = std::end(star_ids) - std::begin(star_ids);
	for( int i = 0; i < star_count; i++){

		int id = ethalon_star.train_oids[i];
		ethalon_star.value.x += feature_1(objects_id_image, id) / star_count;
		ethalon_star.value.y += feature_2(objects_id_image, id) / star_count;
	}
	printf("ethalon_star = (%f, %f)\n", ethalon_star.value.x, ethalon_star.value.y);


	Etalon ethalon_rectangle;
	ethalon_rectangle.name = "rectangle";
	ethalon_rectangle.train_oids = {9, 10, 11, 12};
	int rectangle_count = std::end(rectangle_ids) - std::begin(rectangle_ids);
	for( int i = 0; i < rectangle_count; i++){

		int id = ethalon_rectangle.train_oids[i];
		ethalon_rectangle.value.x += feature_1(objects_id_image, id) / rectangle_count;
		ethalon_rectangle.value.y += feature_2(objects_id_image, id) / rectangle_count;
	}
	printf("ethalon_rectangle = (%f, %f)\n", ethalon_rectangle.value.x, ethalon_rectangle.value.y);

	vector<Etalon> etalons = {ethalon_square, ethalon_star, ethalon_rectangle};
	return etalons;

}

Mat Color_etalons(Mat &objects_id_image, vector<Etalon> trained_etalons) {

	Mat color = Mat (objects_id_image.rows, objects_id_image.cols, CV_32FC3);

	for (int i = 0; i < trained_etalons.size(); i++) {
		Etalon current_etalon = trained_etalons[i];
		for (int j = 0; j < current_etalon.train_oids.size(); j++) {
			int current_oid = current_etalon.train_oids[j];

			for (int y = 0; y < objects_id_image.rows; y++) {
				for (int x = 0; x < objects_id_image.cols; x++) {
					int val = objects_id_image.at<uchar>(y, x);
					if (val == current_oid){
						color.at<Vec3f>(y, x) = current_etalon.brush;
					}
				}
			}

		}
	}
	
	return color;
}



Mat color_etalons(Mat &objects_id_image) {

	Mat color = Mat (objects_id_image.rows, objects_id_image.cols, CV_32FC3);

	int square_count = std::end(square_ids) - std::begin(square_ids);
	for( int i = 0; i < square_count; i++){
		int id = square_ids[i];

		for (int y = 0; y < objects_id_image.rows; y++) {
			for (int x = 0; x < objects_id_image.cols; x++) {
				int val = objects_id_image.at<uchar>(y, x);
				if (val == id){
					color.at<Vec3f>(y, x) = square_brush;
				}
			}
		}
	}

	int star_count = std::end(star_ids) - std::begin(star_ids);
	for( int i = 0; i < star_count; i++){
		int id = star_ids[i];

		for (int y = 0; y < objects_id_image.rows; y++) {
			for (int x = 0; x < objects_id_image.cols; x++) {
				int val = objects_id_image.at<uchar>(y, x);
				if (val == id){
					color.at<Vec3f>(y, x) = star_brush;
				}
			}
		}
	}

	int rectangle_count = std::end(rectangle_ids) - std::begin(rectangle_ids);
	
	for( int i = 0; i < rectangle_count; i++){
		int id = rectangle_ids[i];

		for (int y = 0; y < objects_id_image.rows; y++) {
			for (int x = 0; x < objects_id_image.cols; x++) {
				int val = objects_id_image.at<uchar>(y, x);
				if (val == id){
					color.at<Vec3f>(y, x) = rectangle_brush;
				}
			}
		}
	}

	

	return color;

}

vector<Point2f> train_etalons(Mat & objects_id_image) {
	
	int square_ids[] = {1, 2, 3, 4};
	int star_ids[] = {5, 6, 7, 8};
	int rectangle_ids[] = {9, 10, 11, 12};


	Point2f ethalon_square = Point2f(0,0);
	int square_count = std::end(square_ids) - std::begin(square_ids);
	for( int i = 0; i < square_count; i++){

		int id = square_ids[i];
		ethalon_square.x += feature_1(objects_id_image, id) / square_count;
		ethalon_square.y += feature_2(objects_id_image, id) / square_count;
	}

	printf("ethalon_square = (%f, %f)\n", ethalon_square.x, ethalon_square.y);


	Point2f ethalon_star = Point2f(0,0);
	int star_count = std::end(star_ids) - std::begin(star_ids);
	for( int i = 0; i < star_count; i++){

		int id = star_ids[i];
		ethalon_star.x += feature_1(objects_id_image, id) / star_count;
		ethalon_star.y += feature_2(objects_id_image, id) / star_count;
	}

	printf("ethalon_star = (%f, %f)\n", ethalon_star.x, ethalon_star.y);


	Point2f ethalon_rectangle = Point2f(0,0);
	int rectangle_count = std::end(rectangle_ids) - std::begin(rectangle_ids);
	for( int i = 0; i < rectangle_count; i++){

		int id = rectangle_ids[i];
		ethalon_rectangle.x += feature_1(objects_id_image, id) / rectangle_count;
		ethalon_rectangle.y += feature_2(objects_id_image, id) / rectangle_count;
	}

	printf("ethalon_rectangle = (%f, %f)\n", ethalon_rectangle.x, ethalon_rectangle.y);

	std::vector<Point2f> ethalons = {ethalon_square, ethalon_star, ethalon_rectangle};

	return ethalons;

}

float distance(Point2f a, Point2f b) {
	return (float)(cv::norm(a-b));
	Point2f diff = b - a;
	return sqrtf(diff.x * diff.x + diff.y * diff.y);
}

int find_closest_etalon(std::vector<Point2f> trained_etalons, Point2f test_etalon ){
	float min_distance = 100000000;
	int closest_etalon_id = 999;

	for(int i = 0; i < 3; i++) {
		Point2f train_etalon = trained_etalons[i];
		float dist = distance(test_etalon, train_etalon);
		printf("Distance = %f\n", dist);
		if (dist < min_distance){
			min_distance = dist;
			closest_etalon_id = i;
		}
	}

	return closest_etalon_id;
}


Mat identify_by_features(Mat & objects_id_image_test, std::vector<Point2f> trained_etalons) {
	
	Mat color = Mat (objects_id_image_test.rows, objects_id_image_test.cols, CV_32FC3);

	int test_obj_count = 9;
	for( int i = 0; i < test_obj_count; i++){
		int id = i + 1;

		Point2f test_etalon = Point2f(0,0);
		test_etalon.x = feature_1(objects_id_image_test, id);
		test_etalon.y = feature_2(objects_id_image_test, id);
		printf("test_etalon = (%f, %f)\n", test_etalon.x, test_etalon.y);

		int matched_etalon_index = find_closest_etalon(trained_etalons, test_etalon);
		printf("trained match index %d\n", matched_etalon_index);

		for (int y = 0; y < objects_id_image_test.rows; y++) {
			for (int x = 0; x < objects_id_image_test.cols; x++) {
				int val = objects_id_image_test.at<uchar>(y, x);
				if (val == id){
					if(matched_etalon_index == 0)
						color.at<Vec3f>(y, x) = square_brush;
					if(matched_etalon_index == 1)
						color.at<Vec3f>(y, x) = star_brush;
					if(matched_etalon_index == 2)
						color.at<Vec3f>(y, x) = rectangle_brush;
				}
			}
		}
	}

	return color;
	

}

void exercise_3(Mat & input_image) {

	Mat tresholded_image = tresholding(input_image);
	Mat objects_id_image = Mat::zeros(tresholded_image.rows, tresholded_image.cols, CV_8UC1);
    Mat indexed_image = index_flood(tresholded_image, objects_id_image);


	std::vector<Point2f> trained_etalons = train_etalons(objects_id_image);
	Mat colored_etalons = color_etalons(objects_id_image);
	//vector<Etalon> trained_etalon = Train_etalons(objects_id_image);
	//Mat colored_etalons = Color_etalons(objects_id_image, trained_etalon);

	namedWindow("COLORED A", WINDOW_NORMAL);
    imshow( "COLORED A", colored_etalons);


	Mat source_test = imread( "images/test01.png", IMREAD_GRAYSCALE );
	Mat tresholded_image_test = tresholding(source_test);
	Mat objects_id_image_test = Mat::zeros(tresholded_image_test.rows, tresholded_image_test.cols, CV_8UC1);
    Mat indexed_image_test = index_flood(tresholded_image_test, objects_id_image_test);

	Mat colored_etalons_test = identify_by_features(objects_id_image_test, trained_etalons);


	namedWindow("COLORED B", WINDOW_NORMAL);
    imshow( "COLORED B", colored_etalons_test);
	waitKey( 0 ); // wait until keypressed


}


class OId_featurePoint {
	public:
	int oid;	
	Point2f featurePoint;

	OId_featurePoint(int _oid, Point2f _fpoint) {
		oid = _oid;
		featurePoint = _fpoint;
	}
};


vector<Point2f> train_etalons_k_means(Mat & objects_id_image, Mat & color_features) {
	Mat graph_image = Mat::zeros(100, 100, CV_32FC3);
	int number_of_objects = 12;
	int number_of_categories = 3;

	int square_ids[] = {1, 2, 3, 4};
	int star_ids[] = {5, 6, 7, 8};
	int rectangle_ids[] = {9, 10, 11, 12};

	//Draw features
	for( int id = 1; id <= number_of_objects; id++){
		Point2f tmpPoint = Point2f(feature_1(objects_id_image, id), feature_2(objects_id_image, id));
		graph_image.at<Vec3f>(tmpPoint * 100.0f) = {1, 1, 1};
		printf("featurePoint_%d = (%f, %f)\n", id, tmpPoint.x, tmpPoint.y);
	}

	// Generate random centroids
	vector<Point2f> centroids_rnd = {};
	for (int i = 0; i < 3; i++) {
		float x = randZeroToOne();
		Point2f rndCentroid = Point2f(randZeroToOne(), randZeroToOne());
		centroids_rnd.push_back(rndCentroid);
	}

	//Choose random centroid
	vector<Point2f> centroids = {};
	vector<int> usedOids = {};
	for (int i = 0; i < 3; i++) {
		int rndOid = randOneToN(12);
		while (find(usedOids.begin(), usedOids.end(), rndOid) != usedOids.end())
			rndOid = randOneToN(12);
		

		Point2f rndCentroid = Point2f(feature_1(objects_id_image, rndOid), feature_2(objects_id_image, rndOid));
		centroids.push_back(rndCentroid);
		usedOids.push_back(rndOid);
	}

	//final cluster data wil be hýr
	vector<vector<OId_featurePoint>> clusterData = {};

	// k-means primary cycle
	for (int cycle = 0; cycle < 100; cycle++) {
		// Prepare cluster data structure
		clusterData = {}; // reset !
		for (int i = 0; i < number_of_categories; i++) {
			clusterData.push_back({});
		}

		// Assign featurePoint to clusters
		for (int id = 1; id <= number_of_objects; id++) {
			
			Point2f featurePoint = Point2f(feature_1(objects_id_image, id), feature_2(objects_id_image, id));

			// Determine which centroid is the closest one
			float minDistance = 1000000.f;
			float closestIdx = 0;
			for (int centroidIdx = 0; centroidIdx < number_of_categories; centroidIdx++) {
				if (distance(featurePoint, centroids.at(centroidIdx)) < minDistance) {
					minDistance = distance(featurePoint, centroids.at(centroidIdx));
					closestIdx = centroidIdx;
				}
			}

			//Push feature point to cluster of closest centroid
			//clusterData.at(closestIdx).push_back({id, featurePoint});
			clusterData.at(closestIdx).push_back(OId_featurePoint(id, featurePoint));

		}
		
		// Re-calculate centroids positions
		for (int centroidIdx = 0; centroidIdx < number_of_categories; centroidIdx++) {
			Point2f newCentroid = Point2f(0.f, 0.f);

			for (int objectClusterIdx = 0; objectClusterIdx < clusterData.at(centroidIdx).size(); objectClusterIdx++) {
				
				newCentroid.x += clusterData.at(centroidIdx).at(objectClusterIdx).featurePoint.x / clusterData.at(centroidIdx).size();
				newCentroid.y += clusterData.at(centroidIdx).at(objectClusterIdx).featurePoint.y / clusterData.at(centroidIdx).size();
			}

			centroids.at(centroidIdx) = newCentroid;
		}
		
	}
	



	for( int i = 0; i < number_of_categories; i++){
		printf("centroid_%d = (%f, %f) with %d objects\n",i, centroids.at(i).x, centroids.at(i).y, clusterData.at(i).size());
		graph_image.at<Vec3f>(centroids.at(i) * 100.0f) = Vec3f(0.0f, 0.f, 1.f);
		//circle(graph_image, centroids.at(i) * 100.0f, 2, (1.0f, 0.0f, 0.0f), -1);
	}

	Mat graph_image_bigger;
	resize(graph_image, graph_image_bigger, Size(512, 512), INTER_NEAREST);
	namedWindow("centroids", WINDOW_NORMAL);
    imshow( "centroids", graph_image_bigger);
	waitKey( 0 ); // wait until keypressed


	// Color clusters
	for (int centroidIdx = 0; centroidIdx < number_of_categories; centroidIdx++) {
		for (int objectClusterIdx = 0; objectClusterIdx < clusterData.at(centroidIdx).size(); objectClusterIdx++) {
			int oid = clusterData.at(centroidIdx).at(objectClusterIdx).oid;

			for (int y = 0; y < objects_id_image.rows; y++) {
				for (int x = 0; x < objects_id_image.cols; x++) {
					int val = objects_id_image.at<uchar>(y, x);
					if (val == oid){
						color_features.at<Vec3f>(y, x) = colorsArray.at(centroidIdx);
					}
				}
			}
		}
	}

	return centroids;
}

void exercise_4(Mat & input_image) {

	Mat tresholded_image = tresholding(input_image);
	Mat objects_id_image = Mat::zeros(tresholded_image.rows, tresholded_image.cols, CV_8UC1);
    Mat indexed_image = index_flood(tresholded_image, objects_id_image);

	Mat colored_etalons = Mat::zeros(tresholded_image.rows, tresholded_image.cols, CV_32FC3);
	std::vector<Point2f> trained_etalons = train_etalons_k_means(objects_id_image, colored_etalons);
	// = color_etalons(objects_id_image);
	//vector<Etalon> trained_etalon = Train_etalons(objects_id_image);
	//Mat colored_etalons = Color_etalons(objects_id_image, trained_etalon);

	namedWindow("COLORED TRAIN", WINDOW_NORMAL);
    imshow( "COLORED TRAIN", colored_etalons);


	Mat source_test = imread( "images/test01.png", IMREAD_GRAYSCALE );
	Mat tresholded_image_test = tresholding(source_test);
	Mat objects_id_image_test = Mat::zeros(tresholded_image_test.rows, tresholded_image_test.cols, CV_8UC1);
    Mat indexed_image_test = index_flood(tresholded_image_test, objects_id_image_test);

	Mat colored_etalons_test = identify_by_features(objects_id_image_test, trained_etalons);


	namedWindow("COLORED TEST", WINDOW_NORMAL);
    imshow( "COLORED TEST", colored_etalons_test);
	waitKey( 0 ); // wait until keypressed


}




void train(NN* nn)
{
    int n = 1000;
	double ** trainingSet = new double * [n];
	for ( int i = 0; i < n; i++ ) {
		trainingSet[i] = new double[nn->n[0] + nn->n[nn->l - 1]];

		bool classA = i % 2;

		for ( int j = 0; j < nn->n[0]; j++ ) {
			if ( classA ) {			
				trainingSet[i][j] = 0.1 * ( double )rand() / ( RAND_MAX ) + 0.6;
			} else {
				trainingSet[i][j] = 0.1 * ( double )rand() / ( RAND_MAX ) + 0.2;
			}
		}

		trainingSet[i][nn->n[0]] = ( classA )? 1.0 : 0.0;
		trainingSet[i][nn->n[0] + 1] = ( classA )? 0.0 : 1.0;
	}
    
    double error = 1.0;
    int i = 0;
	while(error > 0.001)
    {
		setInput( nn, trainingSet[i%n] );
 		feedforward( nn );
		error = backpropagation( nn, &trainingSet[i%n][nn->n[0]] );
        i++;
		printf( "\rerr=%0.3f", error );
	}
	printf( " (%d iterations)\n", i );

	for ( int i = 0; i < n; i++ ) {
		delete [] trainingSet[i];
	}
	delete [] trainingSet;
}

void test(NN* nn, int num_samples = 10)
{
    double* in = new double[nn->n[0]];
    
    int num_err = 0;
    for(int n = 0; n < num_samples; n++)
    {
        bool classA = rand() % 2;
        
        for ( int j = 0; j < nn->n[0]; j++ ) 
        {
            if ( classA ) 
            {			
                in[j] = 0.1 * ( double )rand() / ( RAND_MAX ) + 0.6;
            } else 
            {
                in[j] = 0.1 * ( double )rand() / ( RAND_MAX ) + 0.2;
            }
        }
        printf("predicted: %d\n", !classA);
        setInput( nn, in, true );

        feedforward( nn );
        int output = getOutput( nn, true );
        if(output == classA) num_err++;
        printf( "\n" );
    }
    double err = (double)num_err / num_samples;
    printf("test error: %.2f\n", err);
}



void train_features_nn(NN* nn, Mat & objects_id_image, Mat & color_train) {

	vector<int> square_ids = {1, 2, 3, 4};
	vector<int> star_ids = {5, 6, 7, 8};
	vector<int> rectangle_ids = {9, 10, 11, 12};

	// 	Prepare features inputs
	// 	INPUT ARCHITECTURE
	// 	-------------------------------------------------
	//	|	F1	 	| 	F2	 	| SQR	| STAR	| RECT 	| 
	//	|	0.313 	| 	0.495 	| 	1 	| 	0 	| 	0 	| 
	//	|	0.982 	| 	0.140 	| 	0 	| 	0 	| 	1 	| 
	// 	-------------------------------------------------

	double ** trainingSet = new double * [12];
	for( int i = 0; i < 12; i++) {
		int id = i + 1;
		trainingSet[i] = new double[5];

		trainingSet[i][0] = feature_1(objects_id_image, id);
		trainingSet[i][1] = feature_2(objects_id_image, id);


		bool isSquare = (std::find(square_ids.begin(), square_ids.end(), id) != square_ids.end());
		bool isStar = (std::find(star_ids.begin(), star_ids.end(), id) != star_ids.end());
		bool isRectangle = (std::find(rectangle_ids.begin(), rectangle_ids.end(), id) != rectangle_ids.end());		

		trainingSet[i][2] = isSquare ? 1.0f : 0.0f;
		trainingSet[i][3] = isStar ? 1.0f : 0.0f;
		trainingSet[i][4] = isRectangle ? 1.0f : 0.0f;

		// COLOR TRAIN
		for (int y = 0; y < objects_id_image.rows; y++) {
			for (int x = 0; x < objects_id_image.cols; x++) {
				int val = objects_id_image.at<uchar>(y, x);
				if (val == id){
					if(isSquare)
						color_train.at<Vec3f>(y, x) = square_brush;
					if(isStar)
						color_train.at<Vec3f>(y, x) = star_brush;
					if(isRectangle)
						color_train.at<Vec3f>(y, x) = rectangle_brush;
				}
			}
		}
	}

    int n = 12;

    double error = 1.0;
    int i = 0;
	while(error > 0.0001)
    {
		setInput( nn, trainingSet[i%n] );
 		feedforward( nn );
		error = backpropagation( nn, &trainingSet[i%n][nn->n[0]] );
        i++;
		printf("(%0.3f, %0.3f) -> (%0.2f, %0.2f, %0.2f)\n",
			nn->in[0], nn->in[1],
			nn->out[0], nn->out[1], nn->out[2]);
		printf("TRAIN PREDICTION  (%0.2f, %0.2f, %0.2f)\n\n", 
			trainingSet[i%n][2], trainingSet[i%n][3], trainingSet[i%n][4]);
		//printf( "\rerr=%0.4f", error );
	}
	printf( " (%d iterations)\n", i );


	for ( int i = 0; i < n; i++ ) {
		delete [] trainingSet[i];
	}
	delete [] trainingSet;
}

void test_features_nn(NN* nn, Mat & objects_id_image_test, Mat & color_test) {

	vector<int> square_ids = {1, 2, 3, 4};
	vector<int> star_ids = {5, 6, 7, 8};
	vector<int> rectangle_ids = {9, 10, 11, 12};

	for( int id = 1; id <= 12; id++) {
		double * input = new double[2];

		input[0] = feature_1(objects_id_image_test, id);
		input[1] = feature_2(objects_id_image_test, id);

		setInput( nn, input, true );
		feedforward( nn );
		int output = getOutput( nn, true );

		switch (output) {
			case 0:
				printf("SQUARE\n");
				break;
			case 1:
				printf("STAR\n");
				break;
			case 2:
				printf("RECT\n");
				break;
		}


		// COLOR RESULTS
		for (int y = 0; y < objects_id_image_test.rows; y++) {
			for (int x = 0; x < objects_id_image_test.cols; x++) {
				int val = objects_id_image_test.at<uchar>(y, x);
				if (val == id){
					if(output == 0)
						color_test.at<Vec3f>(y, x) = square_brush;
					if(output == 1)
						color_test.at<Vec3f>(y, x) = star_brush;
					if(output == 2)
						color_test.at<Vec3f>(y, x) = rectangle_brush;
				}
			}
		}

	}
    
	
}




void excercise_5(Mat & input_image) {
	
	Mat tresholded_image = tresholding(input_image);
	Mat objects_id_image = Mat::zeros(tresholded_image.rows, tresholded_image.cols, CV_8UC1);
    Mat indexed_image = index_flood(tresholded_image, objects_id_image);
	Mat colored_train = Mat::zeros(objects_id_image.rows, objects_id_image.cols, CV_32FC3);

	NN * nn = createNN(2, 6, 3);
    train_features_nn(nn, objects_id_image, colored_train);
    

	namedWindow("COLORED TRAIN", WINDOW_NORMAL);
    imshow( "COLORED TRAIN", colored_train);
    getchar();
    

	Mat source_test = imread( "images/test02.png", IMREAD_GRAYSCALE );
	Mat tresholded_image_test = tresholding(source_test);
	Mat objects_id_image_test = Mat::zeros(tresholded_image_test.rows, tresholded_image_test.cols, CV_8UC1);
    Mat indexed_image_test = index_flood(tresholded_image_test, objects_id_image_test);

	Mat colored_test = Mat::zeros(tresholded_image_test.rows, tresholded_image_test.cols, CV_32FC3);


    test_features_nn(nn, objects_id_image_test, colored_test);


	namedWindow("COLORED TEST", WINDOW_NORMAL);
    imshow( "COLORED TEST", colored_test);
	waitKey( 0 ); // wait until keypressed

	releaseNN( nn );
    
}


Mat get_grayscale_frame(VideoCapture & cap) {
	Mat color_frame, gray_frame;
	cap >> color_frame;

	GaussianBlur( color_frame, color_frame, Size( 3, 3 ), 0, 0 );
	cvtColor(color_frame, gray_frame, COLOR_BGR2GRAY);
	
	return gray_frame;
}

float normal_pdf(float x, float m, float s) {
    static const float inv_sqrt_2pi = 0.3989422804014327;
    float a = (x - m) / s;

    return inv_sqrt_2pi / s * std::exp(-0.5f * a * a);
}


void plotGaussians(vector<Gaussian> gaussians) {
	Mat plot = Mat::zeros(255, 255, CV_32FC3);

	for (int k = 0; k < gaussians.size(); k++) {
		Gaussian gaussian_k = gaussians.at(k);
		for (int x = 0; x < 255; x++) {
			
			float y = normal_pdf(x, gaussian_k.mi, gaussian_k.sigma) * 255.0 * 10.0;
			y = gaussian_k.pdf(x) * 255.0 * 5.0;
			//printf("%f\n", y);
			plot.at<Vec3f>(255 - int(y) - 1, x) = randomColors10[k];
		}	
	}
	
	



	imshow("Plot Gaussians", plot);
}




int exercise_6() {
	VideoCapture cap("dt_passat.mpg");
	Mat frame, mask;
	if(!cap.isOpened()){
		cout << "Error opening video stream or file" << endl;
		return -1;
	}
		
	const int NO_OF_GAUSSIANS = 3;
	const float LEARNING_RATE = 0.035;
	const float TRESHOLD = 0.7;


	frame = get_grayscale_frame(cap);
	// At least K frames to initialize gaussian distributions
    vector<vector<uchar>> initialData(frame.rows * frame.cols);
    uchar pixelVal;
    for(int i = 0; i < NO_OF_GAUSSIANS + 1; ++i){
        int pixelCount = 0;
        for(int r = 0; r < frame.rows; ++r){
            for(int c = 0; c < frame.cols; ++c){
                pixelVal = frame.at<uchar>(r, c);
                initialData.at(pixelCount).push_back(pixelVal);
                pixelCount++;
            }
        }

        frame = get_grayscale_frame(cap);
    }


	// Create MOG for each pixel in the image
    cout << "Creating MOG for each pixel" << endl;
    cout << "Progress: " << endl;
	
	int progressChecker = 10;
    //MOG* pixelMOG = (MOG*) malloc(imgSize*sizeof(MOG));
    vector<MixtureOfGaussians> pixelMOG;
    int pixelCount = 0;
    for(int r = 0; r < frame.rows; ++r){
        for(int c = 0; c < frame.cols; ++c){
            MixtureOfGaussians mog(NO_OF_GAUSSIANS);
            mog.init(initialData.at(pixelCount));
            mog.alpha = LEARNING_RATE;
            pixelMOG.push_back(mog);
            pixelCount++; 


			int percentDone = floor(float(pixelCount) / float(frame.rows * frame.cols) * 100.0);
			printf("%d\n", percentDone);
        }
    }


	while(true) {
		
		frame = get_grayscale_frame(cap);

		if(frame.empty()) break;

		
		// TEST OPENCV
		//fgbg->apply(frame, mask);

		Mat bgMask = Mat::zeros(frame.rows, frame.cols, CV_8U);
		Mat result = Mat::zeros(frame.rows, frame.cols, CV_8U);

        unsigned int pixelCount = 0;
        // Retrieve pixel values and update MOG
        for(int r = 0; r < frame.rows; ++r){
            for(int c = 0; c < frame.cols; ++c){
                // Get new observation (pixel value)
                pixelVal = frame.at<uchar>(r, c);

                // Update MOG and check if pixel is background or foreground
                pixelMOG[pixelCount].onlineAverage(pixelVal);
                int bgResponse = pixelMOG[pixelCount].predictIsBackground(pixelVal, TRESHOLD);


                if(bgResponse == 1) {
                    bgMask.data[pixelCount] = 0;
                }else if (bgResponse == 0){
                    bgMask.data[pixelCount] = 255;
                }
                pixelCount++;
            }
        }



		// Filter single pixel noise
		

		bitwise_and(frame, frame, result, bgMask);

		int center_pixel = int(pixelCount / 3) + int(frame.cols / 2);
		int right_junction_pixel = int(pixelCount / 3) + frame.cols * 20 + int(frame.cols / 2) + 128;
		int train_pixel = int(pixelCount / 3) + frame.cols * 140 + int(frame.cols / 2) + 288;


		imshow("Frame", frame);
		imshow("Mask", bgMask);
		//imshow("Result", result);

		pixelMOG[train_pixel].isDebug = true;
		plotGaussians(pixelMOG[train_pixel].gaussians);
		
		waitKey(0);
	}




	// TEST

	vector<uchar> dummyData = { 43, 43, 42, 34, 154, 123, 148, 157, 155, 154};

	MixtureOfGaussians mog(4, true);
	mog.init(dummyData);

    plotGaussians(mog.gaussians);
	waitKey();

	mog.onlineAverage(42);
	plotGaussians(mog.gaussians);
	waitKey();

	while (1) {
		mog.onlineAverage(230);
		plotGaussians(mog.gaussians);

		mog.predictIsBackground(230, 0.4);

		waitKey();
	}
}


class Cluster {
	public:
	vector<Point2i> pixel_positions = {};

	Point2i center_position;

	Cluster(){};

};


int exercise_10(const Mat & input_image, int segments) {

	namedWindow("INPUT", WINDOW_NORMAL);
    imshow( "INPUT", input_image);

	int image_size = input_image.cols * input_image.rows;

	// Initial number of pixels in segment (area)
	int segment_size = image_size / segments;
	int S = sqrt(segment_size); // Segment width (assuming square)


	// Init cluster centres
	Mat cluster_map = Mat::zeros(input_image.rows, input_image.cols, CV_8UC1);
	Mat segment_image = Mat::zeros(input_image.rows, input_image.cols, input_image.type());
	vector<vector<float>> clusters_centres = {};
	
	Point2i initial_center = Point2i(S / 2, S / 2);
	Point2i last_center = initial_center;


	Vec3f initial_color = input_image.at<Vec3f>(initial_center);
	clusters_centres.push_back({initial_color[0], initial_color[1], initial_color[2], (float) initial_center.x, (float) initial_center.y});
	printf("Cluster %d (%d, %d)\n", 0, initial_center.x, initial_center.y);
	for (int k = 0; k < segments - 1; k++) {
		
		Point2i new_center = last_center + Point2i(S, 0);

		if(new_center.x > input_image.cols)
			new_center = initial_center + Point2i(0, S);

		Vec3f color = input_image.at<Vec3f>(new_center);

		clusters_centres.push_back({color[0], color[1], color[2], (float) new_center.x, (float) new_center.y});
		last_center = new_center;
		printf("Cluster %d (%d, %d)\n", k, new_center.x, new_center.y);
	}
	

	// Find min gradient in 3x3
	for (int k = 0; k < segments - 1; k++) {
		for (int x = -1; x <= 1; x++) {

			float min_color = INFINITY;
			int min_x, min_y = 0;
			int x_n, y_n = 0;
			for (int y = -1; y <= 1; y++) {
				x_n = clusters_centres[k][3] + x;
				y_n = clusters_centres[k][4] + y;
				Vec3f color = input_image.at<Vec3f>(y_n, x_n);

				if ( ( color[0] + color[1] + color[2]) / 3.0 < min_color) {
					min_color = ( color[0] + color[1] + color[2]) / 3.0;
					min_x = x_n;
					min_y = y_n;
				}
			}	
			clusters_centres[k][3] = x_n;
			clusters_centres[k][4] = y_n;
		}
	}


	int pass = 1;
	while (waitKey( 1 ) != 27) {
		printf("Pass: %d\n", pass++);
		// Recalculate pixels to centres assignment
		for(int y = 0; y < input_image.rows; y++) {
			for(int x = 0; x < input_image.cols; x++) {
				float R = input_image.at<Vec3f>(y, x)[0];
				float G = input_image.at<Vec3f>(y, x)[1];
				float B = input_image.at<Vec3f>(y, x)[2];

				float min_distance = INFINITY;
				float min_cluster_index = 0;
				for (int k = 0; k < segments; k++) {
					
					float R_k = clusters_centres[k][0];
					float G_k = clusters_centres[k][1];
					float B_k = clusters_centres[k][2];
					float x_k = clusters_centres[k][3];
					float y_k = clusters_centres[k][4];

					float m = 0.2;
					float d_RGB = sqrtf( powf(R_k - R, 2) + powf(G_k - G, 2) + powf(B_k - B, 2));
					float d_xy = sqrtf( powf(x_k - x, 2) + powf(y_k - y, 2));
					float D_s = d_RGB + (m / S) * d_xy;

					if(D_s < min_distance) {
						min_distance = D_s;
						min_cluster_index = k;
					}
				}

				cluster_map.at<uchar>(y, x) = min_cluster_index;
				segment_image.at<Vec3f>(y, x) = input_image.at<Vec3f>(clusters_centres[min_cluster_index][4], clusters_centres[min_cluster_index][3]);
			}
		}


		// Recalculate centres
		for (int k = 0; k < segments; k++) {

			int pixels_in_cluster = 0;
			float R_sum = 0.0f;
			float G_sum = 0.0f;
			float B_sum = 0.0f;
			float x_sum = 0.0f;
			float y_sum = 0.0f;

			for(int y = 0; y < input_image.rows; y++) {
				for(int x = 0; x < input_image.cols; x++) {
					if (cluster_map.at<uchar>(y, x) == k) {
						pixels_in_cluster++;
						Vec3f color = input_image.at<Vec3f>(y, x);
						R_sum += color[0];
						G_sum += color[1];
						B_sum += color[2];
						x_sum += (float) x;
						y_sum += (float) y;
					}
				}
			}

			clusters_centres[k][0] = R_sum / (float) pixels_in_cluster;
			clusters_centres[k][1] = G_sum / (float) pixels_in_cluster;
			clusters_centres[k][2] = B_sum / (float) pixels_in_cluster;
			clusters_centres[k][3] = x_sum / (float) pixels_in_cluster;
			clusters_centres[k][4] = y_sum / (float) pixels_in_cluster;
			printf("Cluster %d (%f, %f)\n", k, clusters_centres[k][3], clusters_centres[k][4]);
		}


		namedWindow("SEGMENT IMAGE", WINDOW_NORMAL);
		imshow( "SEGMENT IMAGE", segment_image);
		//int c = waitKey( 0 ); // wait until keypressed
		
	}
}


int main() {
    //Mat source_8uc1 = imread( "images/train.png", IMREAD_GRAYSCALE ); // load color image from file system to Mat variable, this will be loaded using 8 bits (uchar)
    Mat source_8uc1 = imread( "images/bear.png", IMREAD_COLOR ); // load color image from file system to Mat variable, this will be loaded using 8 bits (uchar)
    if (source_8uc1.empty()) {
        printf("Unable to read input file (%s, %d).", __FILE__, __LINE__);
    }

	//Mat input;
	source_8uc1.convertTo(source_8uc1, CV_32FC3, 1.0 / 255.0);

    //namedWindow("Original", WINDOW_NORMAL);
    //imshow( "Original", source_8uc1);
	//moveWindow("Original", 0, 0);

	//exercise_4(source_8uc1);
	//excercise_5(source_8uc1);
	
	
	
	
	exercise_10(source_8uc1, 16);

    return 0;
}
